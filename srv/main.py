import ssl
import argparse
import asyncio
from logging import DEBUG

from aiohttp import web

import chatbu_srv
from chatbu_srv.database import close_pg, init_pg, create_tables
from chatbu_srv.logger import logger, set_logger_level
from chatbu_srv.middlewares import json_middleware, auth_middleware, cors_middleware
from chatbu_srv.models import Message
from chatbu_srv.routes import setup_api_routes
from chatbu_srv.settings import config
from chatbu_srv.socket import sio

parser = argparse.ArgumentParser('chat-bu: twitch extension')
parser.add_argument(
    '--init', action='store_true', help='initialize the database'
)
parser.add_argument(
    '-V', '--version', action='version', version=chatbu_srv.__version__
)
args = parser.parse_args()


async def init_app() -> web.Application:
    app = web.Application()
    sio.attach(app)
    app.on_startup.append(init_pg)
    app.on_cleanup.append(close_pg)

    middlewares = [json_middleware, auth_middleware]
    if config['app']['debug']:
        middlewares.insert(0, cors_middleware)

    api_app = web.Application(middlewares=middlewares)
    setup_api_routes(api_app)

    app.add_subapp('/api/v1', api_app)

    return app


def main():
    if args.init:
        loop = asyncio.get_event_loop()
        loop.run_until_complete(create_tables())

        exit(0)

    sslctx = None
    if config['app']['debug']:
        set_logger_level(DEBUG)
        sslctx = ssl.create_default_context(ssl.Purpose.CLIENT_AUTH)
        sslctx.load_cert_chain('localhost.crt', 'localhost.key')

    app = init_app()
    web.run_app(
        app,
        ssl_context=sslctx,
        port=int(config['app']['port']),
        access_log=logger
    )


if __name__ == '__main__':
    main()
