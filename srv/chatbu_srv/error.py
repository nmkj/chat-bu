def error(code: int, reason: str) -> dict:
    return {'error': code, 'reason': reason}
