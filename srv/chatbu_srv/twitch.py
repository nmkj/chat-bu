import requests
from http import HTTPStatus

from chatbu_srv.settings import config


class TwitchMessenger(object):
    __base_url = 'https://api.twitch.tv/kraken/'

    def __init__(self, client_id: str):
        self.__client_id = client_id
        self.__headers = {
            'Client-ID': self.__client_id,
            'Accept': 'application/vnd.twitchtv.v5+json'
        }

    def is_following(self, user_id: str, channel_id: str) -> bool:
        r = requests.get(
            self.__url('users/%s/follows/channels/%s' % (user_id, channel_id)),
            headers=self.__headers
        )
        return r.status_code == HTTPStatus.NOT_FOUND

    def get_user(self, user_id: str) -> dict:
        r = requests.get(
            self.__url('users/%s' % user_id), headers=self.__headers
        )
        if r.status_code == HTTPStatus.NOT_FOUND:
            return None

        user = r.json()

        return {
            'id': user_id,
            'name': user['name'],
            'display_name': user['display_name'],
        }

    def __url(self, path: str) -> str:
        return requests.compat.urljoin(self.__base_url, path)


twitch = TwitchMessenger(config['app']['client_id'])
