import sys
from logging import INFO, StreamHandler, getLogger

logger = getLogger(__name__)
handler = StreamHandler(stream=sys.stdout)
handler.setLevel(INFO)
logger.setLevel(INFO)
logger.addHandler(handler)
logger.propagate = False


def get_logger():
    return logger


def set_logger_level(level):
    handler.setLevel(level)
    logger.setLevel(level)
