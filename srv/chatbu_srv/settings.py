import os

config = {
    'database': {
        'host': os.getenv('DATABASE_HOST', 'localhost'),
        'port': os.getenv('DATABASE_PORT', '55432'),
        'user': os.getenv('DATABASE_USER', 'chatbu'),
        'password': os.getenv('DATABASE_PASSWORD', 'pgpass'),
        'name': os.getenv('DATABASE_NAME', 'chatbu'),
    },
    'app': {
        'port':
        os.getenv('APP_PORT', '8888'),
        'twitch_secret':
        os.getenv(
            'APP_TWITCH_SECRET', 'kkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkkk'
        ),
        'client_id':
        os.getenv('APP_CLIENT_ID'),
        'debug': True if os.getenv('APP_DEBUG', False) else False,
    },
}
