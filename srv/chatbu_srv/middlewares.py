import json
from http import HTTPStatus
import base64

import jwt
import requests
from aiohttp import web
from aiohttp.web import Request

from chatbu_srv.logger import logger
from chatbu_srv.settings import config
from chatbu_srv.twitch import twitch
from chatbu_srv.error import error

ALLOWED_HEADERS = ','.join((
    'content-type', 'accept', 'origin', 'authorization', 'x-requested-with',
    'x-csrftoken', 'x-extension-token'
))


def __get_cors_headers(request) -> dict:
    return {
        'Access-Control-Allow-Origin': request.headers.get('Origin', '*'),
        'Access-Control-Allow-Methods': request.method,
        'Access-Control-Allow-Headers': ALLOWED_HEADERS,
        'Access-Control-Allow-Credentials': 'true',
    }


@web.middleware
async def cors_middleware(request: Request, handler):
    headers = __get_cors_headers(request)
    if request.method == 'OPTIONS':
        return web.Response(headers=headers)
    else:
        response = await handler(request)
        response.headers.update(headers)
        return response


@web.middleware
async def json_middleware(request: Request, handler):
    if request.method == 'GET':
        return await handler(request)

    try:
        request['json'] = await request.json()
        return await handler(request)
    except json.JSONDecodeError as err:
        logger.debug(
            '{client} : [JSON Error] {msg}'.format(
                client=request.remote, msg=err.msg
            )
        )
        return web.Response(
            status=HTTPStatus.BAD_REQUEST, reason='requests must be json'
        )


@web.middleware
async def auth_middleware(request: Request, handler):
    ts = config['app']['twitch_secret']
    secret = base64.b64decode(ts)
    if secret is None:
        return web.Response(
            status=HTTPStatus.INTERNAL_SERVER_ERROR,
            reason='twitch shared secret must be set'
        )

    token = request.headers.get('x-extension-token')
    if not token:
        return web.Response(
            status=HTTPStatus.UNAUTHORIZED,
            reason='x-extension-token header must be set'
        )

    try:
        payload = jwt.decode(token, secret)
        sub = payload.get('sub')

        if sub != 'comm':
            user_id = payload.get('user_id')
            channel_id = payload.get('channel_id')
            if user_id is None:
                return web.json_response(
                    error(100, 'Your twitch ID is required'),
                    status=HTTPStatus.FORBIDDEN,
                )

            user = twitch.get_user(user_id)
            if user is None:
                return web.json_response(
                    error(102, 'Cannot read your twitch ID'),
                    status=HTTPStatus.FORBIDDEN
                )

            if not twitch.is_following(user_id, channel_id):
                return web.json_response(
                    error(101, 'You must follow this channel'),
                    status=HTTPStatus.FORBIDDEN
                )

            payload.update(user)
            payload['sub'] = 'comm'

            new_token = jwt.encode(payload, secret, 'HS256')

            return web.Response(status=HTTPStatus.CREATED, body=new_token)

        request['user'] = payload

        return await handler(request)

    except jwt.InvalidSignatureError:
        return web.Response(
            status=HTTPStatus.UNAUTHORIZED, reason='Token signature is invalid'
        )

    except jwt.ExpiredSignatureError:
        raise web.Response(
            status=HTTPStatus.UNAUTHORIZED, reason='Token expired'
        )
