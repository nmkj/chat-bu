from aiohttp.web import Application
from chatbu_srv.api import message


def setup_api_routes(app: Application):
    mr = app.router.add_resource('/messages')
    mr.add_route('POST', message.post_messages)
    mr.add_route('GET', message.get_messages)
