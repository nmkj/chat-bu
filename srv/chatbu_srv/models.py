import enum

from gino import Gino

db = Gino()


@enum.unique
class Role(enum.Enum):
    BROADCASTER = 'broadcaster'
    MODERATOR = 'moderator'
    VIEWER = 'viewer'
    EXTERNAL = 'external'


class Message(db.Model):
    __tablename__ = 'messages'

    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String, nullable=False)
    display_name = db.Column(db.String, nullable=False)
    role = db.Column(db.Enum(Role), nullable=False)
    message = db.Column(db.String, nullable=False)
    posted = db.Column(db.DateTime, nullable=False, index=True)

    def to_dict(self):
        return {
            'name': self.name,
            'display_name': self.display_name,
            'role': self.role.value,
            'message': self.message,
            'posted': self.posted.isoformat(),
        }


message_schema = {
    'message': {
        'type': 'string',
        'maxlength': 500,
    },
}
