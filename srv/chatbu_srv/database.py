from chatbu_srv.logger import logger
from chatbu_srv.models import db
from chatbu_srv.settings import config

_dconf = config['database']


async def init_pg(app=None):
    try:
        await db.set_bind(
            'postgresql://{}:{}@{}:{}/{}'.format(
                _dconf['user'], _dconf['password'], _dconf['host'],
                _dconf['port'], _dconf['name']
            ),
            encoding='utf-8',
            echo=False
        )
        logger.debug('connected the database')

        if app is not None:
            app['db'] = db

    except:
        logger.error('cannot connect the database')
        exit(1)


async def close_pg(app):
    await app['db'].pop_bind().close()
    logger.debug('closed the database bind')


async def create_tables():
    await init_pg()
    await db.gino.create_all()
    logger.info('the database was initialized')
