import socketio
from chatbu_srv.logger import logger
from chatbu_srv.settings import config

siologger = False
if config['app']['debug']:
    siologger = logger

sio = socketio.AsyncServer(logger=siologger)

@sio.on('connect')
async def connected(sid, msg):
    logger.debug('Client connected: %s' % (sid))

@sio.on('disconnect')
async def disconnected(sid):
    logger.debug('Client disconnected: %s' % (sid))
