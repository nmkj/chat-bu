import html
from datetime import datetime

from aiohttp import web
from aiohttp.web import Request

from chatbu_srv.models import Message, Role
from chatbu_srv.socket import sio


async def post_messages(request: Request):
    user = request['user']
    mes = request['json'].get('message')

    if mes is None:
        raise web.HTTPBadRequest(reason='Message cannot be empty')

    mes = mes.strip()

    if not mes:
        raise web.HTTPBadRequest(reason='Message cannot be empty')

    posted = await Message.create(
        name=user['name'],
        display_name=user['display_name'],
        role=Role(user['role']),
        message=html.escape(mes),
        posted=datetime.now()
    )

    await sio.send(posted.to_dict())

    return web.json_response(posted.to_dict())


async def get_messages(request: Request):
    logs = await Message.query.order_by(Message.posted).limit(50).gino.all()

    return web.json_response([log.to_dict() for log in logs])
