#!/bin/sh

set -e

export APP_DEBUG=1

while getopts i OPT
do
  case $OPT in
    "i" ) OPTIONS='--init';;
    \? ) OPTIONS='';;
  esac
done

python main.py $OPTIONS
